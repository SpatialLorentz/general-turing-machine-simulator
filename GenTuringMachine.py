file = open('machine.txt','r')
stateIndex = {'START':[]}
configIndex = {}
line = file.readline()
currentState = 'START'
while(line != ""):
    #how it is broken up:
    #state-tapeHead-rewrite-move-state, delimiters are ','
    line = line.strip()
    line = line.split("-")
    if(line[0] == 'START'):
        currentState = line[1]
        line = file.readline()
        continue
    if(not stateIndex.has_key(line[0])):
        stateIndex[line[0]] = []
        stateIndex[line[0]] = [line[1:5]]
    else:
        stateIndex[line[0]].append(line[1:5])
        
    line = file.readline()

file.close()
file = open('input.txt', 'r')
line = file.read()
ptr = int(line.split(',')[0])
line = "".join(line.split(',')[1:])
nL = []
for item in line:
    nL.append(item)
line = nL
newState = False
while(currentState != 'ACCEPT'):
    #initialize configuration for loopBreaking;
    if(not configIndex.has_key("".join(line[:ptr]) + currentState + "".join(line[ptr:]))):
        configIndex["".join(line[:ptr]) + currentState + "".join(line[ptr:])] = 1
    else:
        print "Infinite Loop found"
        break
    #check if we can transition from current state
    newState = False
    if(currentState == 'REJECT'):
        break
    if(stateIndex.has_key(currentState)):
        for item in stateIndex[currentState]:
            if item[0] == line[ptr]:
                line[ptr] = item[1]
                if(item[2] == 'R'):
                    ptr += 1;
                if(item[2] == 'L'):
                    ptr -=1;
                currentState = item[3]
                newState = True
                break
        if(newState == False):
            currentState = 'REJECT'
    else:
        currentState = 'REJECT'

file.close()

if(currentState == 'ACCEPT'):
    print "Accept"
    print "Output: ", line
    file = open('output.txt','w')
    file.write("".join(line))
    print "File Written at output.txt"
if(currentState == 'REJECT'):
    print "Reject"
    print "Output: ", line
