Download and run the python GenTuringMachine.py file and it will process the machine.txt and input.txt and place the contents (if accept) into output.txt

enjoy.



(Barebones Simulator that can escape local infinite loops).

This python project is the culmination of a boring afternoon.
Aims to show how Turing Machines aren't scary and are quite reasonable.

please note: a small limitation is that the left or right hand side of the tape is not treated as infinite or expandable so make sure you pad your input accordingly if you need the blank space.

Another more delightful note: The simulator will be able to detect infinite loops within local space. Such that any input that causes any turing machine to loop over already computed configurations will be detected and rejected.



Contact Perrenspence@gmail.com for more information.